FROM node


RUN groupadd -g 999 appuser && \
    useradd -r -u 999 -g appuser appuser
    
WORKDIR /usr/src/app

COPY ./src/nodejs/package*.json ./

RUN npm install

COPY ./src/nodejs/src ./src

USER appuser

CMD ["npm", "run", "app"]