# Create cache entries if absent for environment variables
macro(UNIFY_ENVVARS_AND_CACHE VAR)
	if ((NOT DEFINED ${VAR}) AND (NOT "$ENV{${VAR}}" STREQUAL ""))
		set(${VAR} "$ENV{${VAR}}" CACHE STRING "${VAR}" FORCE)
	endif()
endmacro()

# Functions to find the unicode libraries
if("${ICU_INCLUDE_DIR}" STREQUAL "")
	message(STATUS "No ICU include directory specified")
endif()

UNIFY_ENVVARS_AND_CACHE(ICU_INCLUDE_DIR)
UNIFY_ENVVARS_AND_CACHE(ICU_LIBRARY_DIR)

if("${ICU_LIBRARY_DIR}" STREQUAL "")
	message(STATUS "No ICU library directory specified")
	find_library(icu NAMES icuuc icuucd PATHS ${ICU_LIBRARY_DIR})
else()
	find_library(icu NAMES icuuc icuucd PATHS ${ICU_LIBRARY_DIR} NO_DEFAULT_PATH)
endif()

if(icu)
	get_filename_component(ICU_LIBRARY_DIR ${icu} PATH)

	add_definitions(-DHAVE_ICU)
	message(STATUS "ICU libraries found")
	# NOTE icudata appears to be icudt on Windows/MSVC and icudata on others
	#      dl is included to resolve dlopen and friends symbols
	
	find_library(icudt NAMES icudata PATHS ${ICU_LIBRARY_DIR} NO_DEFAULT_PATH)
	find_library(icui18n NAMES icui18n PATHS ${ICU_LIBRARY_DIR} NO_DEFAULT_PATH)
	find_library(dl NAMES dl)
	set(ICU_LIBRARIES ${icu} ${icudt} ${dl} ${icui18n})
else()
	message(FATAL_ERROR "UNICODE_SUPPORT enabled, but unable to find ICU. Disable UNICODE_SUPPORT or fix ICU paths to proceed.")
endif()

# Function to link between sub-projects
function(add_dependent_subproject subproject_name)
	#if (NOT TARGET ${subproject_name}) # target unknown
	if(NOT PROJECT_${subproject_name}) # var unknown because we build only this subproject, bimos must have been built AND installed
		find_package(${subproject_name} CONFIG REQUIRED)
	else () # we know the target thus we are doing a build from the top directory
		include_directories(../${subproject_name}/include)
	endif ()
endfunction(add_dependent_subproject)

# Make sure we tell the topdir CMakeLists that we exist (if build from topdir)
get_directory_property(hasParent PARENT_DIRECTORY)
if(hasParent)
	set(PROJECT_${PROJECT_NAME} true PARENT_SCOPE)
endif()
