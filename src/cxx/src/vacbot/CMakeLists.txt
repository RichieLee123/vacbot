cmake_minimum_required(VERSION 3.9)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/../cmake) # Top level directory
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake) # Project specific cmake directory
set(CMAKE_CXX_STANDARD 11) # Set the C++ 11 standard, this could be moved up to make it standard for all projects. Safer here atm
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -Wall -g -O0")

# Make useful CMake variables
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../lib")

# Set up the project within this folder
project("vacbot")

set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)
set(VERSION_PATCH 0)
set(VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

include(Common)

# Enable Git
find_package(Git QUIET)
set(VCS_REVISION "-1")
if(GIT_FOUND)
	include(GetGitRevisionDescription)
	get_git_head_revision(GIT_REFSPEC GIT_SHA1)
	message(STATUS "GIT branch ${GIT_REFSPEC}")
	message(STATUS "GIT revision ${GIT_SHA1}")
	set(VCS_REVISION ${GIT_SHA1})
endif()

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREAD ON)
set(Boost_NO_BOOST_CMAKE ON) # Don't do a find_package in config before searching for a regular install

find_package(Boost COMPONENTS unit_test_framework program_options system filesystem REQUIRED)
if(Boost_FOUND)
	include_directories("${Boost_INCLUDE_DIRS}")
endif()

include_directories(
	${PROJECT_SOURCE_DIR}/include
	${PROJECT_BINARY_DIR}/include)

enable_testing()
add_subdirectory(include)
add_subdirectory(src)
add_subdirectory(doc)
add_subdirectory(test)

# System Packaging
include(CPackConfig)

# CMake Packaging
include(PackageConfigurator)
