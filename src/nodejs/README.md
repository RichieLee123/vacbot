# README #

This is setup and running of the vacbot both locally and in a docker container

### What is this repository for? ###

* Discord VacBot
* 0.0.1

### How do I get set up locally? ###

* Install Python 2.7.x
* clone repo
* `npm install`
* Set your correct environment variables in the created `config.json` file
* `npm run app`

### running in docker container

* `docker build -t <containerTag> .`
* `docker run -it <containerTag> bash`

### Running tests
* `npm test`

### Contribution guidelines ###

* try and wrap new code in unit tests where possible. this shits slightly messy right now but putting more shit on top of shit is never the way
* Submit a PR and have at least 1 approval before merge.