arrayToObj = (array, keyValueMap) => {
    let obj = {};
    for (let i = 0; i < array.length; i++) {
        let curVal = array[i];
        let keyValuePair = keyValueMap(curVal, i, array);
        if (keyValuePair) obj[keyValuePair[0]] = keyValuePair[1];
    }
    return obj;
};

keyValueMap = (curval, index, array) => {
    if (index % 2 === 0) {
        return [curval, array[index+1]];
    }
};

module.exports.convertArraytoObject = array => {
    if (array.length % 2 !== 0) {
        throw new Error('array contains too few/many elements');
    }
    return arrayToObj(array, keyValueMap);
};