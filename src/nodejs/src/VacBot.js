'use strict';


const fs = require('fs');
const Discord = require('discord.js');
const config = require('./config').getConfig;
const SteamAPI = require('./steamAPI');


const prefix = '!';
const client = new Discord.Client();
const steamApi = new SteamAPI();

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./src/commands');
for (const file of commandFiles) {
    const CommandType = require(`./commands/${file}`);
    const command = new CommandType(steamApi);
    console.log(`Adding command ${command.name}`);
    client.commands.set(command.name, command);
}

// Fix unhandled rejection message
process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
  });

client.on('ready', () => {
    console.log(`logged in as ${client.user.tag}!`);
});

client.on('message', message => {
    if (!message.content.startsWith(prefix)) {
        console.log('Dumping non-command message');
        return;
    }

    const args = message.content.slice(prefix.length).split(/ +/);
    const command = args.shift().toLowerCase();
    console.log(command);
    console.log(args);

    if (!client.commands.has(command)) {
        let response = 'Command not supported!\n';
        response += 'Supported commands are:\n';
        for (const commandName of client.commands.values()) {
            response += `${prefix}${commandName.name}\n`;
        }
        message.reply(response);
    }
    else {
        try {
            client.commands.get(command).execute(message, args);
        }
        catch (error) {
            console.error(error);
            message.reply('There was an error executing this command');
        }
    }
});

client.login(config.discord.usertoken);