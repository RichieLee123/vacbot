'use strict';

const internal = require('../internals');

class HelpCommand {
    constructor (steamAPI) {
        internal(this).SteamAPI = steamAPI;
    }

    get name () {
        return 'help';
    }

    get description () {
        return 'Displays a list of supported commands and arguments';
    }

    execute (message, args) {
        console.log('Sending response');
        message.channel.send(`commands for !vacCheck:\n
        -n --name <profileName>\n
        -u --url <steamUrl>\n
        -i --id <steamId>`);
    }
}

module.exports = HelpCommand;