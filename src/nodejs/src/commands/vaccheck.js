'use strict';

const Messenger = require('../messenger');
const watcher = require('../watcher.js');
const SteamAPI = require('../steamAPI');

class VacCheckCommand {
    constructor () {
        this.steamAPI = new SteamAPI();
    }

    get name () {
        return 'vaccheck';
    }

    get description () {
        return 'Checks the specified Steam account for VAC bans';
    }

    execute (message, args) {
        const subcommand = args.shift().toLowerCase();
        switch (subcommand) {
            case '-n':
            case '--name':
                this.steamAPI.getPlayerBansFromPlayerName(args[0])
                .then(bans => {
                    Messenger.printBanDetailsToChannel(bans, message.channel.id);
                });
                break;
            case '-u':
            case '--url':
                this.steamAPI.getPlayerBansFromPlayerUrl(args[0])
                .then(bans => {
                    Messenger.printBanDetailsToChannel(bans, message.channel.id);
                });
                break;
            case '-i':
            case '--id':
                this.steamAPI.getPlayerBansFromSteamId(args[0])
                .then(bans => {
                    Messenger.printBanDetailsToChannel(bans, message.channel.id);
                });
                break;
            case '-w':
            case '--watches':
                watcher.userCheck();
                break;
            default:
                message.channel.send(`subcommand not recognised. try the following:
                -n --name <profileName>
                -u --url <steamUrl>
                -i --id <steamId>
                -w --watches`.replace(/\t/, ''));
        }
    }
}

module.exports = VacCheckCommand;