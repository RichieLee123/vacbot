'use strict';

const DataAccess = require('../db/vacWatchDataAccess');
const arrayToObj = require('../Helpers/arrayToObject');
const SteamAPI = require('../steamAPI');

class VacWatchCommand {
    constructor () {
        this.SteamAPI = new SteamAPI();
    }

    get name () {
        return 'vacwatch';
    }

    get description () {
        return 'adds a watch on a steam user and raises an alert if the user gets banned';
    }

    determineWatchEndDate (monthsToWatch) {
        const watchDate = new Date();
        watchDate.setMonth(watchDate.getMonth()+monthsToWatch);
        return +watchDate;
    }

    execute (message, args) {
        const watchDetails = arrayToObj.convertArraytoObject(args);
        const watchTime = watchDetails.time || 1;
        this.SteamAPI.getSteamIdFromPlayerName(watchDetails.player)
        .then(userId => {
            let watch = {
                user: userId,
                channel: message.channel.id,
                endDate: this.determineWatchEndDate(watchTime),
            };
            DataAccess.addWatch(watch)
            .then(() => {
                message.channel.send(`player ${watchDetails.player} (ID:${watch.user}) added to watch list for ${watchTime} month(s)`);
            })
            .catch(err => {
                console.log(err);
                message.channel.send(`something went wrong adding the watch!`);
            });
        });
    }
}

module.exports = VacWatchCommand;