'use strict';

exports.getConfig = {
        'steam': {
            'api_uri': process.env.STEAM_API_URI,
            'bot_id': process.env.BOT_ID,
            'api_key': process.env.API_KEY,
        },
        'discord': {
            'username': process.env.DISCORD_BOT_USERNAME,
            'usertoken': process.env.DISCORD_BOT_USERTOKEN,
            'secret': process.env.DISCOR_BOT_SECRET,
        },
        'db': {
            'address': `mongodb://${process.env.DBADDRESS || 'mongo'}`,
        },
    };