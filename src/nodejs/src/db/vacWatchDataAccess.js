'use strict';

const mongoClient = require('mongodb').MongoClient;
const config = require('../config').getConfig;

// TODO - pull this out into another place ahd have it pulled in with object.assign
const executeWithConnection = (promiseToExecute, argument) => {
    return new Promise((res, rej) => {
        mongoClient.connect(config.db.address, async (err, connection) => {
            if (err) {return rej(err);}
            try {
                const db = connection.db('vacbot');
                const result = await promiseToExecute(db, argument);
                return res(result);
            }
            catch (error) {return rej(error);}
        });
    });
};

const getActiveWatchDocuments = db => {
    return new Promise((res, rej) => {
        const watchDate = new Date();
        const query = {endDate: {$gt: +watchDate}};
        db.collection('watches').find(query).toArray((err, result) => {
            if (err) {return rej(err);}
            return res(result);
        });
    });
};

const removeWatchDocument = (db, watch) => {
    return new Promise((res, rej) => {
        db.collection('watches').deleteOne(watch, (err, result) => {
            if (err) {return rej(err);}
            return res(result);
        });
    });
};

const addWatchDocument = (db, watch) => {
    return new Promise((res, rej) => {
        db.collection('watches').insertOne(watch, (err, result) => {
            if (err) {return rej(err);}
            return res(result);
        });
    });
};

module.exports = {
    getActiveWatches () {
        return executeWithConnection(getActiveWatchDocuments);
    },

    removeWatch (watch) {
        return executeWithConnection(removeWatchDocument, watch);
    },

    addWatch (watch) {
        return executeWithConnection(addWatchDocument, watch);
    },
};