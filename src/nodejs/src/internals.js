require('weakmap');
let map = new WeakMap();

module.exports = function (object) {
    if (!map.has(object)) {
        map.set(object, {});
    }

    return map.get(object);
};