'use strict';

const config = require('./config').getConfig;
const request = require('request');

module.exports = {
    sendMessageToChannelById (message, channel) {
        const params = {
            url: `https://discordapp.com/api/channels/${channel}/messages`,
            headers: {
                'Authorization': `Bot ${config.discord.usertoken}`,
                'content-type': 'application/json',
            },
            body: JSON.stringify({
                content: message,
            }),
        };
        console.log(params);

        request.post(params, (err, res) => {
            if (err) {console.log(`problem sending message to ${channel}`);}
            console.log(`message sent to channel ${channel}`);
        });
    },

    sendEmbedToChannelById (embed, channel) {
        const params = {
            url: `https://discordapp.com/api/channels/${channel}/messages`,
            headers: {
                'Authorization': `Bot ${config.discord.usertoken}`,
                'content-type': 'application/json',
            },
            body: JSON.stringify({embed: embed}),
        };

        request.post(params, (err, res) => {
            if (err) {console.log(`problem sending message to ${channel}`);}
            console.log(`message sent to channel ${channel}`);
        });
    },

    sendPlayerBannedAlert (details, channelId) {
        const embed = {
            color: 3447003,
            author: {
                name: 'VacBot',
            },
            title: 'A suspect has been banned!',
            url: details.playerUrl,
            description: 'someone on the watch list has been banned!',
            fields: [{
                name: 'Details',
                value: `player: ${details.playerUrl}
            *Community Banned:* ${details.CommunityBanned}
            *VacBanned:* ${details.VACBanned}
            *Number of Vac Bans:* ${details.NumberOfVACBans}
            *Days Since Last Ban:* ${details.DaysSinceLastBan}
            *Game Bans(Overwatch):* ${details.NumberOfGameBans}`.replace(/\t+/g, ''),
            }],
            timestamp: new Date(),
        };

        this.sendEmbedToChannelById(embed, channelId);
    },

    printBanDetailsToChannel (details, channelId) {
        const embed = {
            color: 3447003,
            author: {
                name: 'VacBot',
            },
            url: `http://steamcommunity.com/id/${details.SteamId}`,
            description: 'Ban Details',
            fields: [{
                name: 'Details',
                value: `player: ${details.playerUrl}
            *Community Banned:* ${details.CommunityBanned}
            *VacBanned:* ${details.VACBanned}
            *Number of Vac Bans:* ${details.NumberOfVACBans}
            *Days Since Last Ban:* ${details.DaysSinceLastBan}
            *Game Bans(Overwatch):* ${details.NumberOfGameBans}`.replace(/\t+/g, ''),
            }],
            timestamp: new Date(),
        };

        this.sendEmbedToChannelById(embed, channelId);
    },
};