'use strict';

const request = require('request');
const internal = require('./internals');
const config = require('./config').getConfig;

class SteamAPI {
  constructor () {
    internal(this).steamAPIkey = config.steam.api_key;
    internal(this).steamUrl = config.steam.api_uri;
  }

  getPlayerBansFromPlayerName (playerName) {
    return new Promise((res, rej) => {
      this.getSteamIdFromPlayerName(playerName)
        .then(steamID => this.getPlayerBansFromSteamId(steamID))
        .then(playerbans => {
          return res(playerbans);
        })
        .catch(err => {
          if (err.message === 'No match') {
            return rej(new Error(`user not found with name ${playerName}`));
          }
          return rej(err);
        });
    });
  }

  getPlayerBansFromPlayerUrl (PlayerUrl) {
    return new Promise((res, rej) => {
      const steamRegex = new RegExp(/(?:https?:\/\/)?steamcommunity\.com\/(?:id)\/[a-zA-Z0-9]+/);
      if (!steamRegex.test(PlayerUrl)) {
        return rej(new Error(`url ${PlayerUrl} is not a valid steam player url`));
      }
      PlayerUrl = PlayerUrl.replace(/(?:https?:\/\/)?steamcommunity\.com\/(?:id)\//, '');
      const playerName = PlayerUrl.replace('/', '');
      this.getPlayerBansFromPlayerName(playerName)
        .then(bans => {
          bans.playerUrl = PlayerUrl;
          return res(bans);
        })
        .catch(err => {
          return rej(err);
        });
    });
  }

 getPlayerBansFromSteamId (steamId) {
    return new Promise((res, rej) => {
      request.get(`${internal(this).steamUrl}/ISteamUser/GetPlayerBans/v1/?key=${internal(this).steamAPIkey}&steamids=${steamId}`,
      (err, response, body) => {
        if (err) {
          return rej(err);
        }
        const parsedResponse = JSON.parse(body).players;
        if (parsedResponse.length < 1) {
          return rej(new Error(`could not find ban status for steamId ${steamId}`));
        }
        let bans = parsedResponse[0];
        bans.playerUrl = `https://steamcommunity.com/profiles/${steamId}`;
        return res(bans);
      });
    });
  }

  getSteamIdFromPlayerName (playerName) {
    return new Promise((res, rej) => {
      if (playerName === '') {
        return rej(new Error('no playerName specified'));
      }
      const url = `${internal(this).steamUrl}/ISteamUser/ResolveVanityURL/v0001/?key=${internal(this).steamAPIkey}&vanityurl=${playerName}`;
      request.get(url, (err, response) => {
        if (err) {
          return rej(err);
        }
        const parsedResponse = JSON.parse(response.body).response;
        if (parsedResponse.success !== 1) {
          return rej(new Error(parsedResponse.message));
        }
        return res(JSON.parse(response.body).response.steamid);
      });
    });
  }

  getPlayerNameFromSteamId (steamId) {
    return new Promise((res, rej) => {
      request.get(`${internal(this).steamUrl}/ISteamUser/GetPlayerSummaries/v0002/?key=${internal(this).steamAPIkey}&steamids=${steamId}`,
      (err, response) => {
        if (err) {
          return rej(error);
        }
        const formattedResponse = JSON.parse(response.body).response.players;
        if (formattedResponse.length < 1) {
          return rej(new Error(`no players found for steamId ${steamId}`));
        }
        return res(formattedResponse[0].personaname);
      });
    });
  }

  getfakeBanDeets (steamid) {
    return Promise.resolve({
        SteamId: steamid,
        CommunityBanned: false,
        VACBanned: true,
        NumberOfVACBans: 1,
        DaysSinceLastBan: 7,
        NumberOfGameBans: 0,
        EconomyBan: 'none',
        playerUrl: `https://steamcommunity.com/profiles/${steamid}`,
      });
  }
}

module.exports = SteamAPI;