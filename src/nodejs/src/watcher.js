'use strict';

const DataAccess = require('./db/vacWatchDataAccess');
const SteamApi = require('./steamAPI');
const messenger = require('./messenger');

const playerHasRecentlyBeenBanned = bandata => bandata.DaysSinceLastBan < 7 && bandata.DaysSinceLastBan > 0;

module.exports.userCheck = function userCheck () {
        const steamApi = new SteamApi();
        DataAccess.getActiveWatches()
            .then(watches => {
                watches.forEach(watch => {
                    steamApi.getPlayerBansFromSteamId(`${watch.user}`)
                        .then(bans => {
                            if (playerHasRecentlyBeenBanned(bans)) {
                                messenger.sendPlayerBannedAlert(bans, watch.channel);
                                DataAccess.removeWatch(watch)
                                .catch(err => {
                                    console.log(err);
                                    messenger.sendMessageToChannelById('Error removing watch from watchList', watch.channel);
                                });
                            }
                        });
                });
            });
    };